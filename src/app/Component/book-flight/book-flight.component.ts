import { FlightDataService } from './Data/flight-data.service';
import { Flight } from './Code/flight';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
@Component({
  selector: 'app-book-flight',
  templateUrl: './book-flight.component.html',
  styleUrls: ['./book-flight.component.css']
})
export class BookFlightComponent implements OnInit {
  public Book:Flight;
  public Book_Form:FormGroup;
  public Books:Array<Flight>;
  public Book_Type_Data:Array<Type_Data>
  public Book_Location_Data:Array<Type_Data>
  constructor( private fb:FormBuilder,private FlightService:FlightDataService)
  {
    this.Book=new Flight("","","","",0,new Date(),0,0,new Date())
    this.Book_Form = this.fb.group(
      {
        Book_FullName:["",[Validators.required]],
        Book_From:["",[Validators.required]],
        Book_To:["",[Validators.required]],
        Book_Type:["",[Validators.required]],
        Book_Adults:[0,[Validators.required,Validators.pattern("[0-9]+")]],
        Book_Children:[0,[Validators.required,Validators.pattern("[0-9]+")]],
        Book_Infants:[0,[Validators.required,Validators.pattern("[0-9]+")]],
        Book_Departure:[new Date,[Validators.required]],
        Book_Arrival:[new Date,[Validators.required]]
      }
    )
    this.Books = this.FlightService.getData()
    this.Book_Type_Data =  [{name:"One way",value:"One way"},{name:"Return",value:"Return"}]
    this.Book_Location_Data = [
      {name:"Thailand",value:"Thailand"},
      {name:"Japan",value:"Japan"},
      {name:"South Korea",value:"South Korea"},
      {name:"Malaysia",value:"Malaysia"},
      {name:"Cambodia",value:"Cambodia"},
      {name:"India",value:"India"},
      {name:"Vietnam",value:"Vietnam"},
      {name:"Myanmar",value:"Myanmar"},
      {name:"Singapore",value:"Singapore"},
      {name:"Laos",value:"Laos"},
    ]
  }
  ngOnInit(): void {}
  CheckDate(D:FormGroup):boolean
  {
    let Date1:Date = new Date(D.get("Book_Departure")?.value)
    let Date2:Date = new Date(D.get("Book_Arrival")?.value)
    if(Date2.getTime()<=Date1.getTime())
    {
      return true;
    }
    return false;
  }
  CheckNumber(D:FormGroup):boolean
  {
    if(D.get("Book_Adults")?.value !== null && D.get("Book_Children")?.value !== null && D.get("Book_Infants")?.value !== null)
    {
      if(D.get("Book_Adults")?.value.toString() !== "0")
      {
        return false
      }
      return true
    }
    return true
  }
  CheckLocation(D:FormGroup):boolean
  {
    if(D.get("Book_From")?.value.toString() !== D.get("Book_To")?.value.toString() || D.get("Book_From")?.value.toString() === "" || D.get("Book_To")?.value.toString() === "")
    {
      return false
    }
    return true
  }
  ConverDate(data:Date):string
  {
    return data.toLocaleDateString('th-TH')
  }
  onSubmit(D:FormGroup):void
  {
    let form_record = new Flight(D.get("Book_FullName")?.value,
                                 D.get("Book_From")?.value,
                                 D.get("Book_To")?.value,
                                 D.get("Book_Type")?.value,
                                 D.get("Book_Adults")?.value,
                                 new Date(D.get("Book_Departure")?.value),
                                 D.get("Book_Children")?.value,
                                 D.get("Book_Infants")?.value,
                                 new Date(D.get("Book_Arrival")?.value))
    this.FlightService.addData(form_record)
  }
}
interface Type_Data
{
  name:String,
  value:String
}
