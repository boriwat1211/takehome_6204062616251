import { Data } from './data';
import { Flight } from './../Code/flight';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FlightDataService {
  Flight_Data:Array<Flight>
  constructor() {
    this.Flight_Data = Data.Data_All;
  }
  getData():Array<Flight>
  {
    return this.Flight_Data;
  }
  addData(D:Flight):void
  {
    this.Flight_Data.push(D);
  }
}
