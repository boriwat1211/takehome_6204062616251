import { Flight } from './../Code/flight';
export class Data {
  public static Data_All:Array<Flight> = [
    {
      fullName:"Tle",
      from:"Thailand",
      to:"South Korea",
      type:"One way",
      departure:new Date('09-26-2020'),
      arrival:new Date('09-28-2020'),
      adults:2,
      children:0,
      infants:0,
    },
    {
      fullName:"Tony",
      from:"Japan",
      to:"Thailand",
      type:"Return",
      departure:new Date('02-10-2021'),
      arrival:new Date('02-12-2021'),
      adults:2,
      children:0,
      infants:1,
    },
  ]
}
